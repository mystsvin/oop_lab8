#include "stdafx.h"
#include "figure.h"
#include "ball.h"
#include "paral.h"

figure * figure::In(ifstream &ifst)
{
	figure *add = NULL;
	int key;
	ifst >> key;
	if (key == 1)
	{
		add = new ball();
		add->InElement(ifst);
	}
	else if (key == 2)
	{
		add = new paral();
		add->InElement(ifst);
	}

	return add;
}